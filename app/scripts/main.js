Date.prototype.getMonthName = function() {
  var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  return monthNames[this.getMonth()]
}

Date.prototype.getDayName = function() {
  var dayName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thrusday', 'Friday', 'Saturday'];
  return dayName[this.getDay()]
}

String.prototype.padZero = function(len, c) {
  var s = '',
    c = c || '0',
    len = (len || 2) - this.length;
  while (s.length < len) s += c;
  return s + this;
}

Number.prototype.padZero = function(len, c) {
  return String(this).padZero(len, c);
}

Date.prototype.changeNoonFormat = function() {
  var h = this.getHours(),
    m = this.getMinutes();
  return (h > 12) ? (h - 12 + ':' + m + ' PM') : (h + ':' + m + ' AM');
}

Date.daysBetween = function(date1, date2) {
  // Convert both dates to milliseconds
  var date1_ms = date1.getTime();
  var date2_ms = date2.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;
  //take out milliseconds
  difference_ms = difference_ms / 1000;
  var seconds = Math.floor(difference_ms % 60);
  difference_ms = difference_ms / 60;
  var minutes = Math.floor(difference_ms % 60);
  difference_ms = difference_ms / 60;
  var hours = Math.floor(difference_ms % 24);
  var days = Math.floor(difference_ms / 24);

  return [days, hours, minutes, seconds];
}

Date.subtractDate = function(currentTime, days) {
  var _MS_PER_DAY = 1000 * 60 * 60 * 24;
  var startDate = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), '00', '00', '00', '0');
  startDate.setTime(startDate.getTime() - ((days * _MS_PER_DAY) - _MS_PER_DAY));
  return startDate
}

function Channel(datum) {
  this.id = datum.id;
  this.lebara_id = datum.lebara_id;
  this.name = datum.name;
  this.synopsis = datum.synopsis;
  this.image = datum.image;
  this.epgs = [];
  this.upLimit = null;
  this.downLimit = null;
}

function Epg(datum) {
  this.id = datum.id;
  this.title = datum.title;
  this.image = datum.image;
  this.start_at = datum.start_at;
  this.end_at = datum.end_at;
  this.description = datum.description;
  this.append = false;
}

var HtEpg = function($) {

  // Varible define
  var options = {
      days: 7,
      elem: '.container',
      channelUrl: '/api/v2/channels?per_page=100',
      epgUrl: '/api/v2/epgs/filter',
      callBackUrl: '/epgs/'
    },
    timeWidth = 279,
    channelWidth = 200,
    channelTotalWidth = 0,
    channels = [],
    carouselPosition = 0,
    currentTime = null,
    fetchedDays = [],
    html = [];

  // Service functions
  var get = {

    channels: function channels() {
      var that = this;
      return $.get(options.channelUrl, function(data) {
        data.forEach(function(datum) {
          if (datum.published) {
            var c = new Channel(datum)
            that.push(c)
          }
        });
      });
    },

    epgs: function channels(url) {
      var that = this;
      return $.get(url, function(data) {
        data.forEach(function(datum) {
          var e = new Epg(datum);
          e.channel_id = that.lebara_id;
          that.epgs.push(e)
        });
      });
    },

  }

  // Private functions
  function extendDefaults(properties) {
    var property;
    for (property in properties) {
      if (properties.hasOwnProperty(property)) {
        options[property] = properties[property];
      }
    }
  }

  function puts(obj) {
    console.log(obj)
  }

  function dateFormate(timeString) {
    var date = timeString.split('T');
    var time = date[1].split(':');
    date = date[0].split('-');
    return new Date(date[0], date[1] - 1, date[2], time[0], time[1]);
  }

  function setDate() {
    currentTime = new Date();
    options.startDate = Date.subtractDate(currentTime, options.days);
    options.endDate = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), '23', '59', '59');
  }

  function constructTemplate() {
    html = [];
    channelTotalWidth = (options.days * 24 * timeWidth) + (options.days * 24 + 1) + channelWidth;
    constructDateTemplate();
    constructTimeTemplate();
    html.push('<div class="program-details"><div class="channel-details"></div><div class="epg-details"></div></div>');
    $(options.elem).html(html.join(''));
    constructChannelTemplate();
  }

  function constructDateTemplate() {
    var date, startAt = new Date(options.startDate.getTime()),
      endAt = new Date(options.endDate.getTime());
    html.push('<div class="channel-day"><ul>')
    for (var da = startAt; da <= endAt; da.setDate(da.getDate() + 1)) {
      date = new Date(da);
      var current = (endAt.getDate() === date.getDate()) ? 'selected' : '';
      html.push('<li class="' + current + '"><p>' + date.getDayName() + '</p><span>' + date.getDate().padZero(2) + '</span></li>')
    }
    html.push('</ul></div>');
  }

  function constructTimeTemplate() {
    var time = options.days * 24,
      presentTime = time - (24 - currentTime.getHours()),
      startAt = 12,
      period = 'AM',
      timeCount = 1;
    html.push('<div class="channel-time"><ul><li style="left: 0;width:' + channelWidth + 'px"></li>')
    for (var d = 0; d <= time - 1; d++) {
      var current = (d === presentTime) ? 'selected' : '';
      html.push('<li class="' + current + '"><p>' + startAt.padZero(2) + ':00 ' + period + '</p></li>');
      if (startAt === 11) {
        period = (period === 'AM') ? 'PM' : 'AM'
      }
      startAt = (startAt === 12) ? 1 : (startAt + 1);
    }
    html.push('</ul><div class="prev"><</div><div class="next">></div></div>');
  }

  function constructChannelTemplate() {
    var channelHtml = []
    var epgHtml = []
    get.channels.call(channels).done(function() {
      channelHtml.push('<ul>');
      channels.forEach(function(channel) {
        channelHtml.push('<li><img src=' + channel.image + '>' + channel.name + '</li>');
        epgHtml.push('<div class="channel-row channel' + channel.id + '"><ul><li style="width: ' + channelWidth + 'px"></li></ul></div>');
      });
      channelHtml.push('</ul>');
      $('.channel-details').css('width', channelWidth + 'px').html(channelHtml.join(''));
      $('.epg-details').css('width', channelTotalWidth + 'px').html(epgHtml.join(''));
      $('.channel' + channels[channels.length - 2].id).next().andSelf().addClass('bottom-row');
      constructEpgTemplate(currentTime);
    });
  }

  function constructEpgTemplate(date) {
    var f = '' + date.getDate() + date.getMonth()
    if (fetchedDays.indexOf(f) == -1) {
      fetchedDays.push(f)
      channels.forEach(function(channel) {
        updateEpg(channel, date);
      });
    }
  }

  function updateEpg(channel, date) {
    var utcDateFormat = function utcDateFormat(date) {
      return date.getFullYear() + '-' + (date.getMonth() + 1).padZero() + '-' + date.getDate().padZero() + 'T' + date.getHours().padZero() + ':' + date.getMinutes().padZero() + ':00Z';
    }
    var params = {
      start_at_gteq: utcDateFormat(new Date(date.getFullYear(), date.getMonth(), date.getDate(), '00', '00', '00', '00')),
      start_at_lt: utcDateFormat(new Date(date.getFullYear(), date.getMonth(), date.getDate(), '23', '59', '59', '00')),
      per_page: 100,
      only_required: true,
      channel_id: channel.lebara_id
    }
    var url = options.epgUrl + '?' + decodeURIComponent($.param(params));
    get.epgs.call(channel, url).done(function() {
      appendEpgs(channel);
    })
  }

  function appendEpgs(channel) {
    var epgHtml = [],
      selector = $('.channel' + channel.id);
    channel.epgs.forEach(function(epg) {
      epgHtml.push(epgTemplate(epg))
      epg.append = true;
    });
    var dummy = $('<div/>').append(epgHtml.join(''));
    dummy = addHoverEffect(dummy)
    selector.find('ul').prepend(dummy.find('li'));
  }

  function epgTemplate(epg) {
    if (epg.append) {
      return ''
    }
    var offset = offSetValue(epg.start_at),
      running = '';
    if (fetchedDays.length == 1) {
      var startAt = dateFormate(epg.start_at);
      var endAt = dateFormate(epg.end_at);
      running = (startAt <= currentTime && currentTime < endAt) ? 'class="running"' : '';
    }
    return '<li ' + running + ' style="left: ' + offset + 'px; width: ' + epgWidth(epg.start_at, epg.end_at) + 'px">' + epgDetail(epg) + '</li>';
  }

  function offSetValue(timeString) {
    var time = Date.daysBetween(options.startDate, dateFormate(timeString));
    return calculatePlace(time) + channelWidth;
  }

  function epgDetail(epg) {
    var startDate = new Date(epg.start_at);
    var endDate = new Date(epg.end_at);
    var img = '',
      url = options.callBackUrl + epg.channel_id + '/epg/' + epg.id;
    if (epg.image) {
      img = epg.image.split('.');
      img[img.length - 2] = img[img.length - 2] + '_200x145c';
      img = img.join('.')
    }
    return ['<a class="epg-hover" href=' + url + '>' + epg.title,
      '<div><img src=' + img + ' alt=' + epg.title + '>',
      '<h3>' + epg.title + '</h3>',
      '<p>' + endDate.changeNoonFormat() + ' ' + startDate.changeNoonFormat() + '</p>',
      '<p class="description">' + ((epg.id === epg.description) ? '' : (epg.description.substr(0, 100) + '...')) + '</p>',
      '</div></a>'
    ].join('')
  }

  function epgWidth(startAt, endAt) {
    var time = Date.daysBetween(dateFormate(startAt), dateFormate(endAt));
    return calculatePlace(time)
  }

  function calculatePlace(time) {
    var ltimeWidth = timeWidth + 1;
    return (time[0] * ltimeWidth * 24) + (time[1] * ltimeWidth) + ((ltimeWidth / 60) * time[2])
  }

  // Applying sytle to elements
  function applyStyle() {
    $('.channel-time ul').css('width', channelTotalWidth + 'px');
    $('.channel-time').find('li').not('li:first').css('width', timeWidth + 'px');
  }

  //Click Event triggers
  function eventHandler() {
    $('.channel-time').find('.next').click(next);
    $('.channel-time').find('.prev').click(before);
    $('.channel-day').find('li').click(pickDate);
  }

  function addHoverEffect(obj) {
    obj.find('li').hover(
      function() {
        $(this).addClass('active');
      },
      function() {
        $(this).removeClass('active');
      }
    );
    return obj;
  }

  function triggerUpdateEpg(preload) {
    var oneHour = (timeWidth + 1);
    var oneDay = (24 * oneHour),
      offSetDate = null;
    if (carouselPosition % oneDay === 0) {
      offSetDate = carouselPosition / oneDay
      if (carouselPosition > oneHour && preload) {
        constructEpgTemplate(Date.subtractDate(options.endDate, options.days - offSetDate + 1));
      }
    } else if ((carouselPosition + oneHour) % oneDay === 0) {
      offSetDate = (carouselPosition + oneHour) / oneDay - 1;
    } else if ((carouselPosition - oneHour) % oneDay === 0) {
      offSetDate = (carouselPosition - oneHour) / oneDay;
    }
    if (offSetDate != null && options.days != offSetDate) {
      constructEpgTemplate(Date.subtractDate(options.endDate, options.days - offSetDate));
    }
  }

  // Scroll to live section
  function scrollToLiveSection() {
    var time = Date.daysBetween(options.startDate, currentTime);
    time = [time[0], time[1], 0, 0]
    carouselPosition = calculatePlace(time);
    $('.channel-time ul, .epg-details').css('right', carouselPosition + 'px');
  }

  // Public functions
  function init(arg) {
    if (arg && typeof arg === 'object') {
      extendDefaults(arg);
    }
    setDate();
    constructTemplate();
    applyStyle();
    eventHandler();
    scrollToLiveSection();
    return this;
  }

  function next() {
    if (carouselPosition >= (channelTotalWidth - (timeWidth + (channelWidth * 2)))) {
      return null
    } else {
      carouselPosition = carouselPosition + timeWidth + 1;
    }
    if (carouselPosition % (24 * (timeWidth + 1)) === 0) {
      var i = (carouselPosition / (24 * (timeWidth + 1)));
      if (i <= options.days - 1) {
        $('.channel-day').find('.selected').removeClass('selected').parent().find('li')[i].classList += ' selected'
      }
    }
    triggerUpdateEpg();
    $('.channel-time ul, .epg-details').css('right', carouselPosition + 'px');
  }

  function before() {
    var beforeCaro = null;
    if (carouselPosition <= 0) {
      return null
    } else {
      beforeCaro = carouselPosition
      carouselPosition = carouselPosition - timeWidth - 1;
    }

    if (beforeCaro % (24 * (timeWidth + 1)) === 0) {
      var i = (beforeCaro / (24 * (timeWidth + 1))) - 1;
      if (i >= 0) {
        $('.channel-day').find('.selected').removeClass('selected').parent().find('li')[i].classList += ' selected'
      }
    }
    // if (fetchedDays < options.days) {
    //   if (((options.days - fetchedDays) * (timeWidth + 1) * 24) >= carouselPosition) {
    //     constructEpgTemplate(Date.subtractDate(options.endDate, fetchedDays + 1));
    //   }
    // }
    triggerUpdateEpg(true);
    $('.channel-time ul, .epg-details').css('right', carouselPosition + 'px')
  }

  function pickDate() {
    $(this).siblings().removeClass('selected');
    var index = $(this).parent().find('li').index($(this)[0]);
    var oneDay = (24 * (timeWidth + 1));
    carouselPosition = index * oneDay;
    triggerUpdateEpg();
    $('.channel-time ul, .epg-details').css('right', carouselPosition + 'px');
    $(this).addClass('selected');
  }

  // Return access methods
  return {
    init: init,
    next: next,
    before: before,
    pickDate: pickDate
  };

}(jQuery);

var d = new Date();
var s = HtEpg.init({
  days: 7,
  elem: '.container',
  channelUrl: 'http://localhost:3000/api/v2/channels?per_page=100',
  epgUrl: 'http://localhost:3000/api/v2/epgs/filter',
  callBackUrl: 'http://localhost:3000/channels/'
})
